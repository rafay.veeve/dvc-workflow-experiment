import sys
import random
import os
import yaml
import shutil
from tqdm import tqdm

params = yaml.safe_load(open("params.yaml"))['prepare']

if len(sys.argv) != 2:
    sys.stderr.write("Arguments error. Usage:\n")
    sys.stderr.write("\tpython prepare.py data-file\n")
    sys.exit(1)


# Train/Test split ratio
split = params['split']
random.seed(params['seed'])

input = sys.argv[1]
output_train = os.path.join('prepared_data', 'train')
output_test = os.path.join('prepared_data', 'test')


os.makedirs(output_train, exist_ok=True)
os.makedirs(output_test, exist_ok=True)

print([os.path.join(input, o) for o in os.listdir(input) if os.path.isdir(os.path.join(input,o))])

def copy_to_dst(full_files_names_list, dst):
    for file_name in full_files_names_list:
        shutil.copy(file_name, dst)


for d1 in tqdm(os.listdir(input)):
    print(d1)
    os.makedirs(os.path.join(output_train, d1), exist_ok=True)
    os.makedirs(os.path.join(output_test, d1), exist_ok=True)
    image_names = [os.path.join(input, d1, img_name)
                   for img_name in os.listdir(os.path.join(input, d1))]

    random.shuffle(image_names)

    total_images = len(image_names)

    test_images, train_images = image_names[:int(total_images*split)], image_names[int(total_images*split):]
    # print(train_images)
    # copy the images to the respective dirs
    # first train
    copy_to_dst(train_images, dst=os.path.join(output_train, d1))
    copy_to_dst(test_images, dst=os.path.join(output_test, d1))

print("Done!")

