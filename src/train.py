import sys
import os
import yaml
import torch
import torchvision
import torchvision.transforms as transforms
import torch.utils.data as data
from src.model import Net, Criterion, Optimizer


params = yaml.safe_load(open("params.yaml"))['train']

if len(sys.argv) != 3:
    sys.stderr.write("Arguments error. Usage:\n")
    sys.stderr.write("\tpython prepare.py data_dir model_dir\n")
    sys.exit(1)

# parameters from yaml file
norm_params = params['normalize']
mean = (norm_params['mean']['R'], norm_params['mean']['G'], norm_params['mean']['B'])
std = (norm_params['std']['R'], norm_params['std']['G'], norm_params['std']['B'])
batch_size = params['batch_size']
device = params['device']
lr = params['lr']
momentum = params['momentum']
num_of_epochs = params['epochs']
classes = params['classes']

#-------

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize(mean, std)]
)


prepared_data_path = sys.argv[1]
# load test data
train_data = torchvision.datasets.ImageFolder(root=prepared_data_path,
                                              transform=transform)
train_data_loader = data.DataLoader(train_data,  batch_size=batch_size,
                                    shuffle=True, num_workers=2)


print(train_data)
print(train_data.class_to_idx)

# train the model
training_device = 'cuda:0' if device == 'gpu' else "cpu"
print(training_device)
net = Net(classes)
criterion = Criterion()
optimizer = Optimizer(net.parameters(), lr, momentum)
net.to(device)
print(net)

for epoch in range(num_of_epochs):
    running_loss = 0.0
    for i, data in enumerate(train_data_loader, 0):
        # get inputs, data is a list of [inputs, labels]
        inputs, labels = data[0].to(training_device), data[1].to(training_device)

        # zero the param grads
        optimizer.optimizer.zero_grad()

        # forward + backward + optimizer
        outputs = net(inputs)
        loss = criterion.loss_func(outputs, labels)
        loss.backward()
        optimizer.optimizer.step()

        # print stats
        running_loss += loss.item()

        if i % 10 == 9:  # print every 10 mini-batches
            # dividing running_loss by 10 gives avg running_loss across 10 mini-batches
            print("[{} {}] loss {:.3f}".format(epoch + 1, i + 1,
                                               running_loss / 10))
            running_loss = 0.0

print('Finished Training')

# save model
model_path = sys.argv[2]
os.makedirs(model_path, exist_ok=True)
torch.save(net.state_dict(), model_path+"/model.pth")





