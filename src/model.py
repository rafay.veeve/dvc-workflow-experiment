import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
# define a ConvNet

class Net(nn.Module):
    '''
    (Conv2d -> MaxPool) -> (Conv2d -> MaxPool) -> FC -> FC -> FC
    '''

    def __init__(self, classes):
        super(Net, self).__init__()
        #         self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv1 = nn.Conv2d(3, 12, 5)  # increase width of net
        self.pool = nn.MaxPool2d(2, 2)
        #         self.conv2 = nn.Conv2d(6, 16, 5)
        self.conv2 = nn.Conv2d(12, 16, 5)  # increase width of net
        self.fc1 = nn.Linear(16 * 22 * 22, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, classes)

    def forward(self, x):

        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))

        # flatten
        shape = torch.prod(torch.tensor((x.shape[1:]))).item()
        x = x.view(-1, shape)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)

        return x


# define a loss function
class Criterion:
    def __init__(self):
        self.loss_func = nn.CrossEntropyLoss()


class Optimizer:
    def __init__(self, net_parameters, lr, momentum):
        self.optimizer = optim.SGD(net_parameters, lr, momentum)


