import sys
import os
import json
import torch
import torchvision
import torch.utils.data as data
import torchvision.transforms as transforms
from sklearn.metrics import precision_recall_curve, classification_report
from sklearn.preprocessing import label_binarize
from src.model import Net
import yaml
import numpy as np
from numpy import argmax, vstack

params = yaml.safe_load(open("params.yaml"))['evaluate']

if len(sys.argv) != 5:
    sys.stderr.write('Arguments error. Usage:\n')
    sys.stderr.write('\tpython evaluate.py model test_data scores plots\n')
    sys.exit(1)

# parameters from yaml file
norm_params = yaml.safe_load(open("params.yaml"))['train']['normalize']
mean = (norm_params['mean']['R'], norm_params['mean']['G'], norm_params['mean']['B'])
std = (norm_params['std']['R'], norm_params['std']['G'], norm_params['std']['B'])
batch_size = params['batch_size']
device = params['device']
#-------

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize(mean, std)]
)

# load train data
prepared_data_path = sys.argv[2]
test_data = torchvision.datasets.ImageFolder(root=os.path.join(prepared_data_path),
                                             transform=transform)
test_data_loader = data.DataLoader(test_data, batch_size=batch_size,
                                   shuffle=False, num_workers=2)

# load the model
training_device = 'cuda:0' if device == 'gpu' else "cpu"
print(training_device)
model_path = sys.argv[1]
net = Net(len(test_data.class_to_idx))
print("Number of classes: {}".format(len(test_data.class_to_idx)) )
print(test_data.classes)
net.load_state_dict(torch.load(model_path))
net.to(training_device)


# evaluate the model
probas, predictions, labels = list(), list(), list()

with torch.no_grad():
    for i, (inputs, targets) in enumerate(test_data_loader):
        # print(i)
        # print(targets)
        # evaluate the model on the test set
        yhat = net(inputs)
        probs = torch.softmax(yhat, dim=1)
        # print(yhat)
        # retrieve numpy array
        yhat = yhat.detach().numpy()
        actual = targets.numpy()
        # save probas
        probas.append(probs.detach().numpy())
        # convert to class labels
        yhat = argmax(yhat, axis=1)
        # reshape for stacking
        actual = actual.reshape((len(actual), 1))
        yhat = yhat.reshape((len(yhat),1))
        # store
        predictions.append(yhat)
        labels.append(actual)





probas = vstack(probas)
predictions, labels = vstack(predictions), vstack(labels)
cr = classification_report(labels, predictions, target_names=test_data.classes, output_dict= True)

predictions = list(np.array(predictions).flat)
labels = list(np.array(labels).flat)



# hack to get a len 2 vector when only two classes
predictions = label_binarize(predictions, classes=list(range(len(test_data.class_to_idx)+1)))
labels = label_binarize(labels, classes=list(range(len(test_data.class_to_idx)+1)))

precision = dict()
recall = dict()
thresholds = dict()
average_precision = dict()
c = dict ()
for i in range(len(test_data.class_to_idx)):
    precision[i], recall[i], thresholds[i] = precision_recall_curve(labels[:, i],
                                                                 probas[:, i])


def common_entries(*dcts):
    if not dcts:
        return
    for i in set(dcts[0]).intersection(*dcts[1:]):
        yield (i,) + tuple(d[i] for d in dcts)


scores_file = sys.argv[3]
with open(scores_file, 'w') as fd:
    json.dump(cr, fd)


json_data = dict()
for cl, pr, re, th in common_entries(precision, recall, thresholds):
    json_data[cl] = []
    for p, r, t in zip(pr, re, th):
        json_data[cl].append({'precision': float(p), "recall":float(r), "threshold":float(t)})


plots_dir = sys.argv[4]
os.makedirs(plots_dir, exist_ok=True)
# print(json_data)

for i in json_data.keys():
    with open(os.path.join(plots_dir, test_data.classes[i]+"_prc.json"), 'w') as fd:
        json.dump(json_data, fd)

